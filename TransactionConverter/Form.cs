﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace TransactionConverter
{
    public partial class SqlBatch : Form
    {
        public SqlBatch()
        {
            InitializeComponent();
        }

        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }

        private void SqlBatch_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = folderBrowserDialog1.SelectedPath;
            }
        }
        
        private class Tick
        {
            public Tick(DateTime dt, string line)
            {
                this.dt = dt;
                this.line = line;
            }
            public DateTime dt { get; set; }
            public string line { get; set; }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DirectoryInfo dir = new DirectoryInfo(textBox1.Text);

            var ticks = new List<Tick>();

            button1.Enabled = false;
            button2.Enabled = false;

            foreach(var file in dir.GetFiles("*.txt"))
            {
                var reader = new StreamReader(file.OpenRead());
                var lineCount = File.ReadLines(file.FullName).Count();
                progressBar1.Minimum = 0;
                progressBar1.Maximum = lineCount;
                string line = string.Empty;
                int count = 0;
                label4.Text = file.Name;

                //Get rid of the first line
                reader.ReadLine();

                while((line = reader.ReadLine()) != null)
                {
                    var dt = DateTime.Parse(line.Split(',')[1]);

                    ticks.Add(new Tick(dt, line));

                    ++count;
                    progressBar1.Value = count;
                    Application.DoEvents();
                }
                reader.Close();

            }
            var lines = ticks.OrderBy(u => u.dt).Select(u => u.line).ToList();
            File.WriteAllLines(dir.FullName + "\\trans.csv", lines);

            MessageBox.Show("Process Complete!", "Result", MessageBoxButtons.OK);

            button1.Enabled = true;
            button2.Enabled = true;
        }
    }
}
